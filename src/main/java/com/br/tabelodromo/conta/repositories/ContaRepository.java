package com.br.tabelodromo.conta.repositories;
import com.br.tabelodromo.conta.models.Conta;
import org.springframework.data.repository.CrudRepository;

public interface ContaRepository extends CrudRepository<Conta, Integer> {
}