package com.br.tabelodromo.conta.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Conta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "O nome do titular da conta não pode ser vazio.")
    private String titular;

    @NotNull(message = "O CPF do titular da conta não pode ser vazio.")
    private String documento;

    @NotNull(message = "O número da agência não pode ser vazio.")
    private Integer agencia;

    @NotNull(message = "O número da conta não pode ser vazio.")
    private Integer conta;

    public Conta(Integer id, @NotBlank(message = "O nome do titular da conta não pode ser vazio.") String titular, @NotNull(message = "O CPF do titular da conta não pode ser vazio.") String documento, @NotNull(message = "O número da agência não pode ser vazio.") Integer agencia, @NotNull(message = "O número da conta não pode ser vazio.") Integer conta) {
        this.id = id;
        this.titular = titular;
        this.documento = documento;
        this.agencia = agencia;
        this.conta = conta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

    public Integer getConta() {
        return conta;
    }

    public void setConta(Integer conta) {
        this.conta = conta;
    }
}
